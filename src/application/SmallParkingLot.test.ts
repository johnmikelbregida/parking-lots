import { set } from "date-fns";
import { Car, Motorcycle, Scooter } from "../domain/Vehicles";
import { SmallParkingLot } from "../application/SmallParkingLot";
import { ParkingTicket } from "../domain/ParkingTicket";

describe('Small Parking Lot', () => {

    let parkingLot: SmallParkingLot
    let parkMotorcycle: () => ParkingTicket;
    let parkScooter: () => ParkingTicket;

    beforeEach(() => {
        parkingLot = new SmallParkingLot(2, 0, 0);
        parkMotorcycle = () => parkingLot.park(new Motorcycle(), set(new Date(), {
            date: 29,
            month: 4,
            year: 2022,
            hours: 14,
            minutes: 4,
            seconds: 7
        }));
        parkScooter = () => parkingLot.park(new Scooter(), set(new Date(), {
            date: 29,
            month: 4,
            year: 2022,
            hours: 14,
            minutes: 44,
            seconds: 7
        }));
    })

    it('should instantiate correctly', () => {
        expect(parkingLot.ticketNumber).toBe(0);
        expect(parkingLot.receiptNumber).toBe(0);
        expect(parkingLot.smallSpots.length).toBe(2);
        expect(parkingLot.mediumSpots.length).toBe(0);
        expect(parkingLot.largeSpots.length).toBe(0);
    })

    it('should return a valid ticket when a vehicle parks', () => {
        const result = parkMotorcycle();
        expect(result.print()).toEqual(`Parking Ticket: TicketNumber: 001 Spot Number: 1 Entry Date-time: 29-May-2022 14:04:07`)
    })

    it('should return a valid ticket for consequent parks', () => {
        parkMotorcycle();
        const result = parkScooter();
        expect(result.print()).toEqual(`Parking Ticket: TicketNumber: 002 Spot Number: 2 Entry Date-time: 29-May-2022 14:44:07`);
    })

    it('should throw an error with an appropriate message if the spots are full', () => {
        parkMotorcycle();
        parkScooter();
        expect(() => parkScooter()).toThrow(`No space available.`)
    })

    it('should throw an error with an appropriate message if the parking lot cannot accommodate the vehicle', () => {
        expect(() => parkingLot.park(new Car(), new Date())).toThrow('Parking lot cannot support this vehicle.')
    })

    it('should throw an error with an appropriate message if the vehicle being unparked does not exist', () => {
        expect(() => parkingLot.unpark(new Scooter(), `001`, set(new Date(), {
            date: 29,
            month: 4,
            year: 2022,
            hours: 15,
            minutes: 40,
            seconds: 7
        }))).toThrow(`Parked vehicle does not exist.`)
    })

    it('should return a receipt when a vehicle unparks', () => {
        parkMotorcycle();
        parkScooter();
        const result = parkingLot.unpark(new Scooter(), `002`, set(new Date(), {
            date: 29,
            month: 4,
            year: 2022,
            hours: 15,
            minutes: 40,
            seconds: 7
        }));
        expect(result.print()).toEqual(`Parking Receipt: Receipt Number: R-001 Entry Date-time: 29-May-2022 14:44:07 Exit Date-time: 29-May-2022 15:40:07 Fees: 10`)
    })

    it('should fill out closest open spot when a vehicle parks', () => {
        parkMotorcycle();
        parkScooter();
        parkingLot.unpark(new Scooter(), `002`, set(new Date(), {
            date: 29,
            month: 4,
            year: 2022,
            hours: 15,
            minutes: 40,
            seconds: 7
        }));
        const result = parkingLot.park(new Motorcycle(), set(new Date(), {
            date: 29,
            month: 4,
            year: 2022,
            hours: 15,
            minutes: 59,
            seconds: 7
        }))
        expect(result.print()).toEqual(`Parking Ticket: TicketNumber: 003 Spot Number: 2 Entry Date-time: 29-May-2022 15:59:07`);
    })

    it('should correctly generate a receipt with a fee', () => {
        parkMotorcycle();
        parkScooter();
        parkingLot.unpark(new Scooter(), `002`, set(new Date(), {
            date: 29,
            month: 4,
            year: 2022,
            hours: 15,
            minutes: 40,
            seconds: 7
        }));
        parkMotorcycle();
        const result = parkingLot.unpark(new Motorcycle(), `001`, set(new Date(), {
            date: 29,
            month: 4,
            year: 2022,
            hours: 17,
            minutes: 44,
            seconds: 7
        }))
        expect(result.print()).toEqual(`Parking Receipt: Receipt Number: R-002 Entry Date-time: 29-May-2022 14:04:07 Exit Date-time: 29-May-2022 17:44:07 Fees: 40`);
    })
})