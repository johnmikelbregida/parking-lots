import { add } from "date-fns";
import { ElectricSUV, Motorcycle, SUV } from "../domain/Vehicles";
import {StadiumParkingLot} from "./StadiumParkingLot";

describe('Stadium Parking Lot', () => {

    let parkingLot: StadiumParkingLot = new StadiumParkingLot(1000, 1500, 0);

    it('should handle test scenarios', () => {
        const park1 = parkingLot.park(new Motorcycle(), new Date());
        const receipt1 = parkingLot.unpark(new Motorcycle(), park1.ticketNumber, add(new Date(), { hours: 3, minutes: 40 }))
        expect(receipt1.fees).toEqual(30);

        const park2 = parkingLot.park(new Motorcycle(), new Date());
        const receipt2 = parkingLot.unpark(new Motorcycle(), park2.ticketNumber, add(new Date(), { hours: 14, minutes: 59 }))
        expect(receipt2.fees).toEqual(390);

        const park3 = parkingLot.park(new ElectricSUV(), new Date());
        const receipt3 = parkingLot.unpark(new ElectricSUV(), park3.ticketNumber, add(new Date(), { hours: 11, minutes: 30 }))
        expect(receipt3.fees).toEqual(180);

        const park4 = parkingLot.park(new SUV(), new Date());
        const receipt4 = parkingLot.unpark(new SUV(), park4.ticketNumber, add(new Date(), { hours: 13, minutes: 5 }))
        expect(receipt4.fees).toEqual(580);
        
    })

})