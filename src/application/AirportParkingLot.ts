import { ParkingLot } from "../domain/ParkingLot";
import { VehicleSize } from "../domain/VehicleSize";
import { Vehicle } from "../domain/Vehicle";

export class AirportParkingLot extends ParkingLot {

    private readonly RATES_PER_HOUR = {
        [VehicleSize.Small]: [[1, 0], [8, 40], [24, 60], [25, 80]],
        [VehicleSize.Medium]: [[12, 60], [24, 80], [25, 100]],
        [VehicleSize.Large]: []
    }

    constructor(smallSpots: number, mediumSpots: number, largeSpots: number) {
        super(smallSpots, mediumSpots, largeSpots)
    }

    public getFee(vehicle: Vehicle, hours: number): number {
        let fee = 0;
        Object.values(this.RATES_PER_HOUR[vehicle.size]).forEach((rate, index, array) => {
            if (hours > 0) {
                if (index === array.length - 1) {
                    fee += Math.ceil(hours / 24) * rate[1]
                } else {
                    if (hours < array[index + 1][0]) {
                        fee = rate[1];
                        hours -= rate[0];
                    }
                }
            }
        })
        return fee;
    }
}