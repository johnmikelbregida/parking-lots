import { ParkingLot } from "../domain/ParkingLot";
import { Vehicle } from "../domain/Vehicle";
import { VehicleSize } from "../domain/VehicleSize";

export class MallParkingLot extends ParkingLot {

    private readonly RATES_PER_HOUR = {
        [VehicleSize.Small]: 10,
        [VehicleSize.Medium]: 20,
        [VehicleSize.Large]: 50
    }

    constructor(smallSpots: number, mediumSpots: number, largeSpots: number) {
        super(smallSpots, mediumSpots, largeSpots)
    }

    public getFee(vehicle: Vehicle, hours: number): number {
        return this.RATES_PER_HOUR[vehicle.size] * hours;
    }
}