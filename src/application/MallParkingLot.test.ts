import { add } from "date-fns";
import { Vehicle } from "../domain/Vehicle";
import { Car, Motorcycle, Truck } from "../domain/Vehicles";
import { MallParkingLot } from "./MallParkingLot";

describe('Mall Parking Lot', () => {

    let parkingLot: MallParkingLot = new MallParkingLot(100, 80, 10);

    it('should handle test scenarios', () => {
        const park1 = parkingLot.park(new Motorcycle(), new Date());
        const receipt1 = parkingLot.unpark(new Motorcycle(), park1.ticketNumber, add(new Date(), { hours: 3, minutes: 30 }))
        expect(receipt1.fees).toEqual(40);

        const park2 = parkingLot.park(new Car(), new Date());
        const receipt2 = parkingLot.unpark(new Car(), park2.ticketNumber, add(new Date(), { hours: 6, minutes: 1 }))
        expect(receipt2.fees).toEqual(140);

        const park3 = parkingLot.park(new Truck(), new Date());
        const receipt3 = parkingLot.unpark(new Truck(), park3.ticketNumber, add(new Date(), { hours: 1, minutes: 59 }))
        expect(receipt3.fees).toEqual(100);
    })

})