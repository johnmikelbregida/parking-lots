import { add } from "date-fns";
import { Car, Motorcycle, SUV, Truck } from "../domain/Vehicles";
import { AirportParkingLot } from "./AirportParkingLot";

describe('Airport Parking Lot', () => {

    let parkingLot: AirportParkingLot = new AirportParkingLot(200, 500, 10);

    it('should handle test scenarios', () => {
        const park1 = parkingLot.park(new Motorcycle(), new Date());
        const receipt1 = parkingLot.unpark(new Motorcycle(), park1.ticketNumber, add(new Date(), { minutes: 55 }))
        expect(receipt1.fees).toEqual(0);

        const park2 = parkingLot.park(new Motorcycle(), new Date());
        const receipt2 = parkingLot.unpark(new Motorcycle(), park2.ticketNumber, add(new Date(), { hours: 14, minutes: 59 }))
        expect(receipt2.fees).toEqual(60);
        
        const park3 = parkingLot.park(new Motorcycle(), new Date());
        const receipt3 = parkingLot.unpark(new Motorcycle(), park3.ticketNumber, add(new Date(), { days: 1, hours: 12 }))
        expect(receipt3.fees).toEqual(160);

        const park4 = parkingLot.park(new Car(), new Date());
        const receipt4 = parkingLot.unpark(new Car(), park4.ticketNumber, add(new Date(), { minutes: 50 }))
        expect(receipt4.fees).toEqual(60);

        const park5 = parkingLot.park(new SUV(), new Date());
        const receipt5 = parkingLot.unpark(new SUV(), park5.ticketNumber, add(new Date(), { hours: 23, minutes: 59 }))
        expect(receipt5.fees).toEqual(80);

        const park6 = parkingLot.park(new Car(), new Date());
        const receipt6 = parkingLot.unpark(new Car(), park6.ticketNumber, add(new Date(), { days: 3, hours: 1 }))
        expect(receipt6.fees).toEqual(400);
    })

})