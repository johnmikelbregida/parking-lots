import { ParkingLot } from "../domain/ParkingLot";
import { VehicleSize } from "../domain/VehicleSize";
import { Vehicle } from "../domain/Vehicle";

export class StadiumParkingLot extends ParkingLot {

    private readonly RATES_PER_HOUR = {
        [VehicleSize.Small]: [[4, 30], [12, 60], [100]],
        [VehicleSize.Medium]: [[4, 60], [12, 120], [200]],
        [VehicleSize.Large]: []
    }

    constructor(smallSpots: number, mediumSpots: number, largeSpots: number) {
        super(smallSpots, mediumSpots, largeSpots)
    }

    public getFee(vehicle: Vehicle, hours: number): number {
        let fee = 0;
        Object.values(this.RATES_PER_HOUR[vehicle.size]).forEach((rate, index, array) => {
            if (hours > 0) {
                if (index === array.length - 1) {
                    fee += hours * rate[0]
                } else {
                    fee += rate[1];
                    hours -= index === 0 ? rate[0] : rate[0] - array[index - 1][0];
                }
            }
        })
        return fee;
    }
}