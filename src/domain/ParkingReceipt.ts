import { IPrintable } from "./IPrintable";

export class ParkingReceipt implements IPrintable {
    constructor(
        public receiptNumber: string,
        public entryDateTime: string,
        public exitDateTime: string,
        public fees: number

    ) { }

    print(): string {
        return `Parking Receipt: Receipt Number: R-${this.receiptNumber} Entry Date-time: ${this.entryDateTime} Exit Date-time: ${this.exitDateTime} Fees: ${this.fees}`
    }
}