import { Vehicle } from "./Vehicle";

export interface Spot {
    index: number;
    vehicle: Vehicle;
    ticketNumber: string;
    entryDateTime: Date;
}

export type NullableSpot = Spot | null;