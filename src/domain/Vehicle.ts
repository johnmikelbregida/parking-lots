import { IParkingLot } from "./IParkingLot";
import { NullableSpot } from "./Spot";
import { VehicleSize } from "./VehicleSize";

export abstract class Vehicle {
    constructor(public size: VehicleSize) {}
    abstract determineSpotToUse(parkingLot: IParkingLot): NullableSpot[];
}