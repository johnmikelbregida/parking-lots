export enum VehicleSize {
    Small,
    Medium,
    Large
}