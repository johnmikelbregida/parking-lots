import { IPrintable } from "./IPrintable";

export class ParkingTicket implements IPrintable {
    constructor(
        public ticketNumber: string,
        public spotNumber: number,
        public entryDateTime: string
    ) { }

    print(): string {
        return `Parking Ticket: TicketNumber: ${this.ticketNumber} Spot Number: ${this.spotNumber} Entry Date-time: ${this.entryDateTime}`
    }
}