import { IParkingLot } from "./IParkingLot";
import { NullableSpot } from "./Spot";
import { Vehicle } from "./Vehicle";
import { VehicleSize } from "./VehicleSize";

export class Motorcycle extends Vehicle {
    constructor() {
        super(VehicleSize.Small)
    }
    determineSpotToUse(parkingLot: IParkingLot): NullableSpot[] {
        return parkingLot.smallSpots;
    }
}

export class Scooter extends Vehicle {
    constructor() {
        super(VehicleSize.Small)
    }
    determineSpotToUse(parkingLot: IParkingLot): NullableSpot[] {
        return parkingLot.smallSpots;
    }
}

export class Car extends Vehicle {
    constructor() {
        super(VehicleSize.Medium)
    }
    determineSpotToUse(parkingLot: IParkingLot): NullableSpot[] {
        return parkingLot.mediumSpots;
    }
}

export class SUV extends Vehicle {
    constructor() {
        super(VehicleSize.Medium)
    }
    determineSpotToUse(parkingLot: IParkingLot): NullableSpot[] {
        return parkingLot.mediumSpots;
    }
}

export class ElectricSUV extends Vehicle {
    constructor() {
        super(VehicleSize.Medium)
    }
    determineSpotToUse(parkingLot: IParkingLot): NullableSpot[] {
        return parkingLot.mediumSpots;
    }
}

export class Bus extends Vehicle {
    constructor() {
        super(VehicleSize.Large)
    }
    determineSpotToUse(parkingLot: IParkingLot): NullableSpot[] {
        return parkingLot.largeSpots;
    }
}

export class Truck extends Vehicle {
    constructor() {
        super(VehicleSize.Large)
    }
    determineSpotToUse(parkingLot: IParkingLot): NullableSpot[] {
        return parkingLot.largeSpots;
    }
}

