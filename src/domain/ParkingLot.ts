import { differenceInMinutes, format } from "date-fns";
import { IParkingLot } from "./IParkingLot";
import { ParkingReceipt } from "./ParkingReceipt";
import { ParkingTicket } from "./ParkingTicket";
import { NullableSpot, Spot } from "./Spot";
import { Vehicle } from "./Vehicle";


export abstract class ParkingLot implements IParkingLot {

    private readonly DATE_TIME_FORMAT = 'dd-MMMM-yyyy HH:mm:ss';

    ticketNumber = 0;
    receiptNumber = 0;
    smallSpots: NullableSpot[];
    mediumSpots: NullableSpot[];
    largeSpots: NullableSpot[];

    constructor(smallSpots: number, mediumSpots: number, largeSpots: number) {
        this.smallSpots = Array.from({ length: smallSpots }, () => null);
        this.mediumSpots = Array.from({ length: mediumSpots }, () => null);
        this.largeSpots = Array.from({ length: largeSpots }, () => null);
    }


    public park(vehicle: Vehicle, entryDateTime: Date): ParkingTicket {
        const spots = vehicle.determineSpotToUse(this);
        if (spots.length === 0) throw new Error("Parking lot cannot support this vehicle.");
        const freeSpot = this.getFreeSpot(spots);
        if (freeSpot < 0) throw new Error("No space available.");
        this.ticketNumber++;
        const newSpot: Spot = {
            index: freeSpot,
            vehicle,
            ticketNumber: String(this.ticketNumber).padStart(3, '0'),
            entryDateTime,
        };
        this.useSpot(newSpot, freeSpot, spots)
        return new ParkingTicket(newSpot.ticketNumber, newSpot.index + 1, this.formatDateTime(newSpot.entryDateTime))
    }

    public unpark(vehicle: Vehicle, ticketNumber: string, exitDateTime: Date): ParkingReceipt {
        const spots = vehicle.determineSpotToUse(this);
        const index = spots.findIndex(x => x?.ticketNumber === ticketNumber);
        if (index < 0) throw new Error("Parked vehicle does not exist.")
        const parking = spots[index];
        const fees = this.getFee(parking?.vehicle!, Math.ceil(differenceInMinutes(exitDateTime, parking?.entryDateTime!) / 60));
        spots.splice(index, 1, null);
        this.receiptNumber++;
        return new ParkingReceipt(String(this.receiptNumber).padStart(3, '0'), this.formatDateTime(parking?.entryDateTime!), this.formatDateTime(exitDateTime), fees)
    }

    private getFreeSpot(spots: NullableSpot[]) {
        return spots.findIndex(x => x === null);
    }

    private useSpot(spot: Spot, index: number, spots: NullableSpot[]) {
        spots.splice(index, 1, spot)
    }

    private formatDateTime(dateTime: Date): string {
        return format(dateTime, this.DATE_TIME_FORMAT).toString();
    }

    abstract getFee(vehicle: Vehicle, hours: number): number;
}