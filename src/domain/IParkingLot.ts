import { ParkingReceipt } from "./ParkingReceipt";
import { ParkingTicket } from "./ParkingTicket";
import { NullableSpot } from "./Spot";
import { Vehicle } from "./Vehicle";

export interface IParkingLot {
    ticketNumber: number;
    receiptNumber: number;
    smallSpots: NullableSpot[];
    mediumSpots: NullableSpot[];
    largeSpots: NullableSpot[];
    park: (vehicle: Vehicle, entryDateTime: Date) => ParkingTicket;
    unpark: (vehicle: Vehicle, ticketNumber: string, exitDateTime: Date) => ParkingReceipt;
}