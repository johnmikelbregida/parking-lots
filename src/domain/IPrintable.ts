export interface IPrintable {
    print(): string;
}