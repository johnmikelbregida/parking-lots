# Parking Lot Exercise

### To run the tests, do the following in order

1. If you have [volta](https://volta.sh), the project will pick it up and configure the Node version where this was tested on.
2. Do `npm i` to install the project dependencies
3. Simply run `npm test` to see the test results